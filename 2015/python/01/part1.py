"""
https://adventofcode.com/2015/day/1
"""

with open('input', 'r') as f:
    data = list(f.readline())

# convert parens to integers representing floor movement, then take the sum.
# the sum of all movements is the final floor
result = sum(map(lambda x: {'(': 1, ')': -1}[x], data))

print(result)