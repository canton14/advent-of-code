"""
https://adventofcode.com/2015/day/1
"""

with open('input', 'r') as f:
    data = list(f.readline())

# convert parens to integers representing floor movement
data = map(lambda x: {'(': 1, ')': -1}[x], data)

# keep track of current floor
floor = 0

for i, f in enumerate(data):
    # add the current movement to the current floor
    floor += f

    # stop when you hit the basement
    if floor == -1: break

# increment to account for 0 indexing
result = i + 1

print(result)