"""
https://adventofcode.com/2015/day/2
"""
from math import prod
with open('input', 'r') as f:
    # import the input as a list of lists of box dimensions as integers,
    data = [list(map(int, l.strip().split('x'))) for l in f.readlines()]

total = sum(2 * sum(sorted(box)[:2]) + prod(box) for box in data)

print(total)