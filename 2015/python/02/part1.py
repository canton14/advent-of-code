"""
https://adventofcode.com/2015/day/2
"""
from itertools import combinations
from math import prod

with open('input', 'r') as f:
    # import the input as a list of lists of box dimensions
    data = [list(map(int, l.strip().split('x'))) for l in f.readlines()]

total = 0

for box in data:
    # calculate the area of each side
    sides = list(map(prod, combinations(box, 2)))

    # this is the formula in the problem, so keep a running total for each box
    total += 2 * sum(sides) + min(sides)

print(total)