"""
https://adventofcode.com/2015/day/3
"""
from collections import Counter

with open('input', 'r') as f:
    # map the character inputs into vectors representing the coordinate movement
    def movements(x):
        return {
            '^': (0, 1),
            '>': (1, 0),
            'v': (0, -1),
            '<': (-1, 0)
        }[x]

    data = list(map(movements, f.readline()))

current_houses = [(0,0), (0,0)]
visited_houses = Counter()

# the zip will return a list of tuples of two movement pairs,
# the first is for santa, the second for robo santa
for movements in zip(data[::2], data[1::2]):
    visited_houses.update(current_houses)
    # now we have to update both houses at once
    current_houses = [
        (
            current_houses[0][0] + movements[0][0],
            current_houses[0][1] + movements[0][1]
        ),
        (
            current_houses[1][0] + movements[1][0],
            current_houses[1][1] + movements[1][1]
        )
    ]

print(len(visited_houses))