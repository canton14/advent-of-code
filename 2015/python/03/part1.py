"""
https://adventofcode.com/2015/day/3
"""
from collections import Counter

with open('input', 'r') as f:
    # map the character inputs into vectors representing the coordinate movement
    def movements(x):
        return {
            '^': (0, 1),
            '>': (1, 0),
            'v': (0, -1),
            '<': (-1, 0)
        }[x]

    data = list(map(movements, f.readline()))

current_house = (0,0)
visited_houses = Counter()

for movement in data:
    visited_houses.update([current_house])
    current_house = (
        current_house[0] + movement[0],
        current_house[1] + movement[1]
    )

print(len(visited_houses))