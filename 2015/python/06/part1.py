"""
https://adventofcode.com/2015/day/6
"""
import re

with open('input', 'r') as f:
    instructions = [re.search(r'(\w+) (\d+),(\d+) through (\d+),(\d+)', line).groups() for line in f.readlines()]

grid = [[0 for _ in range(1000)] for _ in range(1000)]

for i in instructions:
    for x in range(int(i[1]), int(i[3]) + 1):
        for y in range(int(i[2]), int(i[4]) + 1):
            if i[0] == 'on':
                grid[x][y] = 1
            elif i[0] == 'off':
                grid[x][y] = 0
            elif i[0] == 'toggle' and grid[x][y] == 0:
                grid[x][y] = 1
            elif i[0] == 'toggle' and grid[x][y] == 1:
                grid[x][y] = 0


print(sum(sum(row) for row in grid))
