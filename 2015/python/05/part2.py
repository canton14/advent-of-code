"""
https://adventofcode.com/2015/day/5
"""
import re

with open('input', 'r') as f:
    lines = f.readlines()

nice_lines = 0

for line in lines:
    # these are the conditions in the problem description, respectively
    if (
        re.search(r'([a-z][a-z])[a-z]*\1', line) and
        re.search(r'([a-z])[a-z]\1', line)
    ):
        nice_lines += 1

print(nice_lines)


