"""
https://adventofcode.com/2015/day/5
"""
import re

with open('input', 'r') as f:
    lines = f.readlines()

nice_lines = 0

for line in lines:
    # these are the conditions in the problem description, respectively
    if (
        len(re.findall(r'[aeiou]', line)) >= 3 and
        re.search(r'([a-z])\1', line) and
        not re.search('(ab|cd|pq|xy)', line)
    ):
        nice_lines += 1

print(nice_lines)