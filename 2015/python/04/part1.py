"""
https://adventofcode.com/2015/day/4
"""
import hashlib

data = 'iwrupvqb'

n = 5

i = 1

while not hashlib.md5(f'{data}{i}'.encode()).hexdigest().startswith('0' * n):
    i += 1

print(i)