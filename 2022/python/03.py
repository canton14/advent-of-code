from math import floor

with open('2022/input/03.txt', 'r') as f:
    sacks = [l.strip() for l in f.readlines()]


def ordinal(item):
    o = ord(next(iter(item)))

    return o - 96 if o > 96 else o - 38


# Part 1

total = 0

for sack in sacks:
    front = set(sack[:floor(len(sack)/2)])
    back = set(sack[floor(len(sack)/2):])

    item = front.intersection(back)

    total += ordinal(item)

print(total)

# Part 2

total = 0

for i in range(0, len(sacks), 3):
    item = set(sacks[i]).intersection(set(sacks[i+1])).intersection(set(sacks[i+2]))

    total += ordinal(item)

print(total)