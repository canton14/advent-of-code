import re

with open('2022/input/05.txt', 'r') as f:
    lines = f.readlines()

    # hold the stacks of containers
    stacks_1 = [[] for _ in range(10)]
    stacks_2 = [[] for _ in range(10)]

    # parsing shenanigens to build the stacks
    for line in lines[7::-1]:
        for i, c in enumerate(range(1, len(line), 4)):
            if line[c] != ' ':
                # add to proper stack, enumerate i to account for 1 indexing in problem
                stacks_1[i + 1].append(line[c])
                stacks_2[i + 1].append(line[c])

    instructions = [tuple(map(int, re.findall(r'\d+', line))) for line in lines[10:]]

# part 1
for i in instructions:
    n = i[0]
    f = stacks_1[i[1]]
    t = stacks_1[i[2]]

    t.extend(reversed(f[len(f) - n:len(f)]))

    del f[len(f) - n:len(f)]


print(''.join(stack.pop() for stack in stacks_1[1:]))

# part 2
for i in instructions:
    n = i[0]
    f = stacks_2[i[1]]
    t = stacks_2[i[2]]

    t.extend(f[len(f) - n:len(f)])

    del f[len(f) - n:len(f)]


print(''.join(stack.pop() for stack in stacks_2[1:]))

    


