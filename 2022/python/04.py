import re

with open('2022/input/04.txt', 'r') as f:
    assignments = [list(map(int, re.findall(r'\d+', l))) for l in f.readlines()]

# part 1

total_1 = 0
total_2 = 0

for a in assignments:
    # part 1
    if a[0] >= a[2] and a[1] <= a[3] or a[0] <= a[2] and a[1] >= a[3]:
        total_1 += 1
    
    # part 2
    if a[0] <= a[2] and a[1] >= a[2] or a[2] <= a[0] and a[3] >= a[0]:
        total_2 += 1

print(total_1, total_2)