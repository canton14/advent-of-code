with open('2022/input/02.txt', 'r') as f:
    guide = [line.strip() for line in f.readlines()]

# part 1
scores = {
    'A X': 4,
    'A Y': 8,
    'A Z': 3,
    'B X': 1,
    'B Y': 5,
    'B Z': 9,
    'C X': 7,
    'C Y': 2,
    'C Z': 6
}

print(sum(scores[game] for game in guide))

# part 2
scores = {
    'A X': 3,
    'A Y': 4,
    'A Z': 8,
    'B X': 1,
    'B Y': 5,
    'B Z': 9,
    'C X': 2,
    'C Y': 6,
    'C Z': 7
}

print(sum(scores[game] for game in guide))
