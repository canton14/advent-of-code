from collections import defaultdict

with open('2022/input/07.txt', 'r') as f:
    lines = f.readlines()

# store sizes
file_system = defaultdict(int)
current_dir = []

# part 1
for line in lines:
    parts = line.split()

    # update current directory
    if parts[0] == '$' and parts[1] == 'cd':
        if parts[2] == '..':
            current_dir.pop()
        else:
            current_dir.append(parts[2])

    # if we're looking at a file, add the size to all parent directories 
    # and the current directory
    if parts[0] not in ['$', 'dir']:
        for i in range(len(current_dir)):
            file_system['/'.join(current_dir[:i+1])] += int(parts[0])

print(sum(d for d in file_system.values() if d <= 100000))

# # part 2


required_space = file_system['/'] - 40_000_000

for k,v  in file_system.items():
    print(k, v, required_space, v >= required_space)


print(list((k, v) for k, v in file_system.items() if v >= required_space))
print(
    min(
        ((k, v) for k, v in file_system.items() if v >= required_space),
        key=lambda t: t[1]
    )
)