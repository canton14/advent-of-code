with open('2022/input/01.txt', 'r') as f:
    lines = [[int(n) for n in s.split('\n') if n != ''] for s in f.read().split('\n\n')]

# part 1
print(max(sum(line) for line in lines))

# part 2
print(sum(sorted(sum(line) for line in lines)[-3:]))
