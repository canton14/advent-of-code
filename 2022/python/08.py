from itertools import product

with open('2022/input/08.txt', 'r') as f:
    # rows = tuple(tuple(map(int, tuple(l.strip()))) for l in f.readlines())

    rows = (
        (3, 0, 3, 7, 3),
        (2, 5, 5, 1, 2),
        (6, 5, 3, 3, 2),
        (3, 3, 5, 4, 9),
        (3, 5, 3, 9, 0),
    )

    columns = tuple(zip(*rows))

total = 0

for x, y in product(range(len(rows)), range(len(columns))):
    print(x, y)

print(total)