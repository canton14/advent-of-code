with open('2022/input/06.txt', 'r') as f:
    stream = f.readline()

# part 1
for i in range(4, len(stream) + 1):
    if len(stream[i-4:i]) == len(set(stream[i-4:i])):
        print(i)
        break

# part 2
for i in range(14, len(stream) + 1):
    if len(stream[i-14:i]) == len(set(stream[i-14:i])):
        print(i)
        break